===============================================================================
* Este � o arquivo readme.txt. Ele traz um pequeno relat�rio com o enunciado 
e requisitos do trabalho 01 e explica��es de como o programa cumpre estes 
requisitos. O relat�rio, indiretamente, tamb�m ensina a usar o programa.

* O arquivo trb01_mateus_nuno.jar � o execut�vel do programa. Para acion�-lo 
basta digitar em um terminal:
	java -jar trb01_mateus_nuno.jar
	
* No arquivo src.zip podem ser encontrados os arquivos fontes do programa
desenvolvido. O arquivo info.txt possui algumas informa��es da implementa��o
do mesmo. O arquivo polar_retangular.py � um pequeno programa escrito em
Python utilizado para produzir a espiral usada no programa.

===============================================================================
UFSC - CTC - INE - CCO
Disciplina: INE 5379 - Reconhecimento de Padr�es II. 2006/2
Alunos: Mateus
		Nuno 

Trabalho 01

Enunciado / requisitos:
I- Definir 4 conjuntos de dados: 2 qquer (2D e nD) e 2 espirais
II- Espiral: Gerar com e sem ru�do e com n�mero vari�vel de elementos de dados.
III- Implementar Hamming+, DE, NN e kNN
IV- Gerar dados aleat�rios e testar (NN KNN) com todos os 4 conjuntos.
V- Mostrar pela cor com qual categoria foram classificados os dados aleat�rios 
	(interface gr�fica). 
VI- Implementar diagrama de Voronoi 
VII- Gerar dados aleat�rios e testar (Voronoi) no conj. 2D e nas duas espirais
VIII- Apresentar os resultados com interface gr�fica

Elabora��o:
	Observando a interface gr�fica (GUI) do programa desenvolvido, pode-se
notar que a coluna da direita possui bot�es para gerar os 4 conjuntos de
dados solicitados (requisito I): 
	1- bot�o "2D" para gerar um conjunto qualquer de dados 2D
	2- bot�o "ND" para gerar um conjunto qualquer de dados ND
		(deve-se especificar a dimens�o desejada para os padr�es, quando 
		se clica neste bot�o)
	3- bot�o "Espiral" para gerar uma espiral simples sem ru�do
	4- bot�o "Espiral com ru�do" para gerar uma espiral simples com ru�do
		(deve-se especificar a quantidade desejada de elementos de dados 
		que ir�o constituir a espiral, seja ela com ou sem ru�do) 
	(3 e 4 cumprem o requisito II)
Ainda na coluna da direita temos os bot�es "2D Rand�mico" e "ND Rand�mico",
usados para gerar um certo numero de dados aleat�rios. Se desejar-se mais
dados basta apertar o bot�o seguidamente.

	Quando � gerado um padr�o 2D ele � apresentado na forma de um ponto 
colorido no plano cartesiano, a cor representa a classe do padr�o, sendo 
que a cor preta significa um padr�o ainda n�o classificado. (requisito V)
Quando um padr�o � do tipo ND, ele � apresentado na forma:
	- P(<lista dos valores das dimens�es>, <nome da classe>)
para os padr�es ND quaisquer
	- A(<lista dos valores das dimens�es>, nao classificado)
para os padr�es rand�micos gerados. 
Ap�s serem classificados, os padr�es rad�micos ficam da seguite forma:
	- A(<lista dos valores das dimens�es>, <nome da classe>)

	A coluna da direita possui bot�es relacionados com os algoritmos
implementados:
	- bot�o "NN" para o algoritmo Nearest Neighbour 
	- bot�o "KNN" para o algoritmo K-Nearest Neighbour 
	- bot�o "Voronoi" para a gerar o diagrama de voronoi
	- e adicionalmente o bot�o "Delaunay" para efetuar a triangula��o de
		Delaunay
	Antes de se executar NN ou KNN deve-se selecionar a medida de dist�ncia
	desejada (Hamming ou Euclidiana). Este bot�es de algoritmos e os radio
	buttons para selecionar a medida de dist�ncia cumprem os requisitos
	III e VI.

De acordo com os requisitos IV e VII devem-se efetuar os seguites testes:

	IV- Gerar dados aleat�rios e testar (NN KNN) com todos os 4 conjuntos.
De acordo com IV temos os sequintes testes:
a- NN e KNN sobre 2D e dados 2D rand�micos

b- NN e KNN sobre Espiral sem ru�do e dados 2D rand�micos

c- NN e KNN sobre Espiral com ru�do e dados 2D rand�micos

d- NN e KNN sobre ND e dados ND rand�micos

"a", "b", "c" e "d" representam 16 testes, pois tanto NN quanto KNN podem
ser testados com dist�ncia Hamming e Euclidiana.

Ser� descrito de modo gen�rico como proceder para realizar este testes.
"a", "b" e "c" podem ser testados da seguite maneira:
	* preciona-se o bot�o para gerar os dados: 2D ou espirais
	* preciona-se o bot�o "2D Rand�mico" para gerar os dados a serem classificados
	* seleciona-se o algoritmo desejado para dist�ncia
	* preciona-se o bot�o NN ou KNN para classificar os dados
	
	OBS:
	+ quando precionado KNN � solicitado o valor K, que deve ser menor ou 
	igual ao n�mero de padr�es (os n�o rand�micos) gerados.
	
	+ as duas espirais possuem 2 cores apenas para que se possa testar
	NN e KNN sobre elas.
	
	+ lembre-se os padr�es rand�micos s�o classificados em rela��o aos 
	padr�es originais e n�o a enventualmente aos rand�micos j� classificados
	
"d" pode ser testado de maneira semelhante a "a", "b" e "c", exceto que
no primeiro passo preciona-se o bot�o "ND" e escolhe-se a dimens�o desejada
e no segundo passo preciona-se o bot�o "ND Rand�mico", que gerar� padr�es
da mesma dimens�o dos padr�es de refer�ncia.

VII- Gerar dados aleat�rios e testar (Voronoi) no conj. 2D e nas duas espirais
Para testar Voronoi basta fazer o seguinte:
	* preciona-se o bot�o para gerar os dados: 2D ou espirais
	* preciona-se o bot�o "Voronoi" para gerar o diagrama
	
	OBS:
	+ Em uma m�quina rodando a 800 MHz com 512 MB de RAM, para geral o 
	diagrama de Voronoi em uma espiral sem ru�do de 900 pontos, leva-se
	aproximadamente 3 minutos. Na mesma m�quina a mesma espiral, com 
	450 pontos leva-se aproximadamente 30s e com 200 pontos cerca de 5s.
	Aparentemente o tempo cresce cerca de 6 vezes, sempre que o   
	n�mero de pontos � dobrado.
	
	+ � poss�vel testar tamb�m a triangulariza��o de Delaunay com o bot�o
	"Delaunay", mostrando ou n�o os circulos circunscritos.
	
	+ � poss�vel ainda testar Voronoi e Delaunay sobre dados 2D rand�micos,
	embora este n�o seja um requisito do programa.


Tanto para dados 2D quanto para dados ND, os resultados s�o apresentados na
interface gr�fica (requisito VIII). Para padr�es 2D os dados se apresentam
de forma gr�fica, num plano carteziano e para padr�es ND em uma lista de
padr�es em forma alg�brica. De acordo com o padr�o gerado � chaveado 
automaticamente do modo gr�fico para o alg�brico e virce-versa.

	A qualquer momento pode-se apagar os dados e resultados gerados clicando 
no bot�o "Reset", localizado na parte superior da GUI.
===============================================================================
