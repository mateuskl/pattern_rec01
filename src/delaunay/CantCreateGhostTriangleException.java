package delaunay;

public class CantCreateGhostTriangleException extends Exception {

	public CantCreateGhostTriangleException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CantCreateGhostTriangleException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CantCreateGhostTriangleException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CantCreateGhostTriangleException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
