package delaunay;

import java.util.ArrayList;
import java.util.List;
import pattern.Pattern;
import pattern.Pattern2D;
import geometry.Triangle;
import geometry.Circle;
import geometry.Edge;;

public class DelaunayIncrementalTriangulator implements DelaunayTriangulator {

	private List<Triangle> trianglesSet;
			
	public List<Triangle> triangulize(List<Pattern> patternsSet) {
		trianglesSet = new ArrayList<Triangle>();		
		Triangle ghosthTriangle = null;
		
		try {
			ghosthTriangle = createGhosthTriangle(patternsSet);
			//System.out.println("ghosthTriangle" + ghosthTriangle.toString());
		} catch (CantCreateGhostTriangleException e) {
			System.out.println("Cant Make Delaunay Traangulation: cant create the ghost triangle");
			e.printStackTrace();
		}
		trianglesSet.add(ghosthTriangle);
		
		for (Pattern pattern : patternsSet) {
			addPatternToTrianglesSet(pattern);
		}
		
		removeGhosthTriangleAndOtherIvalidsTriangles(ghosthTriangle);
		
		return trianglesSet;
	}

	// ---
	protected Triangle createGhosthTriangle(List<Pattern> patternsSet) throws CantCreateGhostTriangleException {
		/* cria um triangulo cujo circunc�rculo cont�m todos os padr�es
		 * depois aumenta este triangulo, de modo a cada um de seus
		 * antigos v�rtices fiquem com o valor anterior mais o 
		 * di�metro do circunc�rculo. Dessa forma creio que o tri�ngulo
		 * resultante conter� todos os padr�es.
		 * Repare que os v�rtices do tri�ngulo fantasma est�o 
		 * a - sobre o eixo y
		 * b - na reta y = x
		 * c - na reta y = -x
		 * 
		 * */
		double ay, bx, cx;
		ay = 3.0;
		bx = -1.0;
		cx = 1.0;
		Pattern2D a = new Pattern2D(0.0, ay, "");
		Pattern2D b = new Pattern2D(bx, bx, "");
		Pattern2D c = new Pattern2D(cx, -cx, "");		
		Triangle ghosthTriangle = new Triangle(a, b, c);
		int i = 0;
		int limit = 30;
		int k = 1;
		// make circumCircleOfghosthTriangleContainsAllPatterns
		while (! circumCircleOfghosthTriangleContainsAllPatterns(ghosthTriangle, patternsSet) && (i < limit)) {
			ay += k;
			bx -= k;
			cx += k;
			a = new Pattern2D(0.0, ay, "");
			b = new Pattern2D(bx, bx, "");
			c = new Pattern2D(cx, -cx, "");		
			ghosthTriangle = new Triangle(a, b, c);			
			i++;
		}
		// make ghosthTriangleContainsAllPatterns
		
		double diameter = ghosthTriangle.getCircumCircle().getRadius() * 2; 
		ay += diameter;
		bx -= diameter;
		cx += diameter;
		a = new Pattern2D(0.0, ay, "");
		b = new Pattern2D(bx, bx, "");
		c = new Pattern2D(cx, -cx, "");		
		ghosthTriangle = new Triangle(a, b, c);		
		
		if (i >= limit)
			throw new CantCreateGhostTriangleException(); 
			
		return ghosthTriangle;
	}
	
	private boolean circumCircleOfghosthTriangleContainsAllPatterns(Triangle ghosthTriangle, List<Pattern> patternsSet) {
		Circle circumCircle = ghosthTriangle.getCircumCircle();
		
		for (Pattern pattern : patternsSet) {
			if (! circumCircle.contains(pattern))
				return false;
		}
		return true;		
	}

	// ---
	protected void addPatternToTrianglesSet(Pattern pattern) {
		/* Tri�ngulo incidente (incidentTriangle) � aquele cujo c�rculo
		 * circuscrito cont�m o ponto (Pattern2D) */
		//System.out.println("executando DelaunayIncrementalTriangulator::addPatternToTrianglesSet");
		
		List<Triangle> incidentsTriangles = getIncidentsTriangles(pattern);
		List<Edge> validEdges = getExclusiveOfOneTriangleEdges(incidentsTriangles);
		for (Edge edge : validEdges) {
			trianglesSet.add(new Triangle((Pattern2D) pattern, edge));
		}
		
		for (Triangle incident : incidentsTriangles) {
			trianglesSet.remove(incident);
		}
		
	}
	
	private List<Triangle> getIncidentsTriangles(Pattern pattern) {
		/* Retorna ponteiros para os triangulos incidentes
		 * varre os triangulos e um triangulo verifica se o ponto � incidente
		nele*/
		List<Triangle> incidentsTriangles = new ArrayList<Triangle>();
		
		for (Triangle triangle : trianglesSet) {
			if (triangle.isIncident((Pattern2D) pattern)) {
				// apenas ponteiros...
				incidentsTriangles.add(triangle);
			}
		}
		
		return incidentsTriangles;
	}
	
	private List<Edge> getExclusiveOfOneTriangleEdges(List<Triangle> incidentsTriangles) {
		/* Retorna uma c�pia das arestas v�lidas
		 * Somente as arestas pertencentes a apenas um triangulo 
		 * incidente s�o v�lidas 
		 */
		
		List<Edge> validEdges = new ArrayList<Edge>();
		
		List<Edge> exclusiveEdges;
		List<Triangle> otherIncidents;
		for (Triangle triangle : incidentsTriangles) {
			// c�pia de objeto
			otherIncidents = triangle.getOverYourself(incidentsTriangles);
			// c�pia de objeto 
			exclusiveEdges = triangle.getExclusiveEdges(otherIncidents); 
			if (exclusiveEdges.size() != 0)
				// apenas ponteiros...
				validEdges.addAll(exclusiveEdges);
		}			
		
		return validEdges;
	}

	// ---
	protected void removeGhosthTriangleAndOtherIvalidsTriangles(Triangle ghosthTriangle) {
		List<Triangle> trianglesToRemove = new ArrayList<Triangle>();
		//System.out.println("trianglesSet.size()" + trianglesSet.size());
		for (Triangle triangle : trianglesSet) {
			if (!triangle.equals(ghosthTriangle)) {
				if (triangle.shareVertices(ghosthTriangle)) {
					trianglesToRemove.add(triangle);
					//System.out.println("Adicionando um triangulo para remocao");
				}				
			}
		}
		
		for (Triangle triangle : trianglesToRemove) {
			trianglesSet.remove(triangle);
			//System.out.println("Removendo um triangulo");
		}
		
		trianglesSet.remove(ghosthTriangle);
		//System.out.println("trianglesSet.size()" + trianglesSet.size());
	}
	
	// remover este depois
	
	public  Triangle getCreateGhosthTriangle(List<Pattern> patternsSet) {
		try {
			Triangle t = createGhosthTriangle(patternsSet);
			System.out.println("t" + t.toString());
			System.out.println("cc" + t.getCircumCircle().toString());
			return t;
		} catch (CantCreateGhostTriangleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
}
