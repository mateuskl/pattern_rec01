package delaunay;
import java.util.List;
import pattern.Pattern;
import geometry.Triangle;

public interface DelaunayTriangulator {
	public List<Triangle> triangulize(List<Pattern> patternsSet);
	
}
