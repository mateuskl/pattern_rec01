package geometry;

import pattern.Pattern;
import pattern.Pattern2D;

public class Circle {

	private double radius;
	private Pattern2D center;
	
	public Circle(Pattern2D pa, Pattern2D pb, Pattern2D pc) {
		Pattern2D a = pa;
		Pattern2D b = pb;
		Pattern2D c = pc;
		
		center = createCircumCenter(a, b, c);
		// Can be, a, b, or c...
		radius = center.euclidianDistance(a);	
	}

	private Circle(Pattern2D c, double r) {
		center = c;
		radius = r;
	}

	private Pattern2D createCircumCenter(Pattern2D a, Pattern2D b, Pattern2D c) {
		double crossProduct = crossProduct(a, b, c);  	
		double aS = a.getX() * a.getX() + a.getY() * a.getY();
		double bS = b.getX() * b.getX() + b.getY() * b.getY();
		double cS = c.getX() * c.getX() + c.getY() * c.getY();

		double n = aS * (b.getY() - c.getY()) + bS * (c.getY() - a.getY()) + cS * (a.getY() - b.getY());
		double cx = (n / (2 * crossProduct));
		
		double m = aS * (c.getX() - b.getX()) + bS * (a.getX() - c.getX()) + cS * (b.getX() - a.getX());
		double cy = (m / (2 * crossProduct));

		return new Pattern2D(cx, cy, "");
	}

	private double crossProduct(Pattern2D a, Pattern2D b, Pattern2D c) {
		double u1 =  b.getX() - a.getX();
		double v1 =  b.getY() - a.getY();
		double u2 =  c.getX() - a.getX();
		double v2 =  c.getY() - a.getY();
		return (u1 * v2 - v1 * u2);		
	}

	public boolean contains(Pattern pattern) {
		// menor ou menor igual?
		return pattern.euclidianDistance(center) < radius;
	}

	public double getRadius() {
		return radius;
	}

	public Pattern2D getCenter() {
		return center.clone();
	}
	
	public Circle clone() {
		return new Circle(center.clone(), radius);
	}
	
	public String toString() {
		return " center" + center.toString() + " radius " + radius;
	}
		
}
