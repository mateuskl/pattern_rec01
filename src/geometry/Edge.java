package geometry;

import pattern.Pattern2D; 

public class Edge {

	private Pattern2D v1;
	private Pattern2D v2;
	
	public Edge(Pattern2D a, Pattern2D b) {
		v1 = a;
		v2 = b;	
	}

	public Pattern2D getV1() {
		return v1;
	}

	public Pattern2D getV2() {
		return v2;
	}

	public double size() {
		return v1.euclidianDistance(v2);
	}
	
	public Object clone() {
		return new Edge(v1.clone(), v2.clone());
	}

	public boolean isEquivalent(Edge edge) {
		return (v1.equals(edge.v1) || v1.equals(edge.v2)) && 
			(v2.equals(edge.v1) || v2.equals(edge.v2));
	}
	
	public boolean equals(Object edge) {
		return isEquivalent((Edge) edge);
	}

	public Pattern2D getMiddlePoint() {
		Pattern2D middlePoint;
		double x = (v1.getX() + v2.getX()) / 2;
		double y = (v1.getY() + v2.getY()) / 2;
		middlePoint = new Pattern2D(x,y,"");
		return middlePoint;
	}
	
}
