package geometry;
import java.util.Vector;

public class NDPoint {
	
	public static final int hammingDistance = 0;
	public static final int euclidianDistance = 1;
	
	protected Vector<Double> dimensions;
	protected int currentDimensionValueIndex;
	
	public NDPoint(double[] dimensionsValues) {
		dimensions = new Vector<Double>();
		
		for (int i = 0; i < dimensionsValues.length; ++ i) {
			dimensions.add(new Double(dimensionsValues[i]));
		}
		currentDimensionValueIndex = 0;
	}
	
	public NDPoint() {		
	}

	private int getNumberOfDimensions() {
		return dimensions.size();
	}

	private double getDimensionValue(int i) {
		return ((Double) dimensions.get(i)).doubleValue();		
	}
	
	// DistanceAlgorithms
	
	public double hammingDistance(NDPoint other) {
		double hammingDistance = 0.0;		
		
		assert (getNumberOfDimensions() == other.getNumberOfDimensions());				
		
		for (int i = 0; i < getNumberOfDimensions(); i++) 
			hammingDistance += Math.abs(getDimensionValue(i) - other.getDimensionValue(i));		
		
		return hammingDistance;
	}
	
	public double euclidianDistance(NDPoint other) {
		double euclidianDistance = 0.0;
		double squareEuclidianDistance = 0.0;
		
		assert (getNumberOfDimensions() == other.getNumberOfDimensions());				
		
		for (int i = 0; i < getNumberOfDimensions(); i++) {
			double diffDimensionValues = getDimensionValue(i) - other.getDimensionValue(i);
			squareEuclidianDistance += Math.pow(diffDimensionValues, 2);
		}
		
		euclidianDistance = Math.sqrt(squareEuclidianDistance);
		return euclidianDistance;
	}
	
	// Methods to public access the dimensions values of a point - begin
	public Double firstDimensionValue() {
		currentDimensionValueIndex = 0;
		return getDimensionValue(0);
	}

	public boolean isDone() {
		return currentDimensionValueIndex == getNumberOfDimensions();
	}

	public void nextDimensionValue() {
		currentDimensionValueIndex ++;
	}
	
	public Double currentDimensionValue() {
		return getDimensionValue(currentDimensionValueIndex);
	}	
	// Methods to public access the dimensions values of a point - end
	
	public NDPoint clone() {
		double dimensionsValues[] = new double[dimensions.size()];
		
		for (int i = 0; i < dimensions.size(); ++ i) {
			dimensionsValues[i] = dimensions.get(i).doubleValue();
		}
		
		return new NDPoint(dimensionsValues);
	}
	
	public boolean equals(Object other) {
		NDPoint otherPoint = (NDPoint) other;
		return isEquals(otherPoint);
	}
	
	public boolean isEquals(NDPoint otherPoint) {
		if (dimensions.size() != otherPoint.dimensions.size())
			return false;
		
		for (int i = 0; i < dimensions.size(); ++i ) {
			if (! dimensions.get(i).equals(otherPoint.dimensions.get(i)))
				return false;
		}
		
		return true;
	}
	
}
