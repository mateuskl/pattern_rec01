package geometry;

import java.util.ArrayList;
import java.util.List;
import pattern.Pattern2D;

public class Triangle {
	
	// vertices
	private Pattern2D a;
	private Pattern2D b;
	private Pattern2D c;
	// edges
	private Edge ab;
	private Edge bc;
	private Edge ca;
	// Circumcircle
	private Circle circumCircle;
	
	public Triangle(Pattern2D pa, Pattern2D pb, Pattern2D pc) {
		a = pa;
		b = pb;
		c = pc;
		ab = new Edge(a,b);
		bc = new Edge(b,c);
		ca = new Edge(c,a);
		circumCircle = new Circle(a, b, c);
		verifyInequalityPrinciple();
	}

	public Triangle(Pattern2D pa, Edge edge) {
		a = pa;
		b = edge.getV1();
		c = edge.getV2();
		ab = new Edge(a,b);
		bc = edge;
		ca = new Edge(c,a);
		circumCircle = new Circle(a, b, c);
		verifyInequalityPrinciple();
	}
	
	private void verifyInequalityPrinciple() {
		assert (ab.size() < (bc.size() + ca.size())) && 
			(bc.size() < (ab.size() + ca.size())) &&
			(ca.size() < (ab.size() + bc.size())) : 
			"Violation of Inequality Principle of Triangles";
	}

	public boolean isIncident(Pattern2D pattern) {
	/* Tri�ngulo incidente (incidentTriangle) � aquele cujo c�rculo
	 * circuscrito cont�m o ponto (pattern) */
		return circumCircle.contains(pattern);
	}

	public List<Triangle> getOverYourself(List<Triangle> incidentsTriangles) {
		/* Returns a copy of incidentsTriangles excluding the
		 * object that run this method
		 * */
		List<Triangle> triangles = new ArrayList<Triangle>();
		for (Triangle triangle : incidentsTriangles)
			if (! triangle.equals(this))
				triangles.add((Triangle) triangle.clone());
		return triangles;
	}
	
	public List<Triangle> getNeighbours(List<Triangle> others) {
		/* Returns a copy of neighbours Triangles or
		 * a empty list
		 * */
		List<Triangle> neighbours = new ArrayList<Triangle>();
		for (Triangle triangle : others)
			if (shareEdge(triangle))
				neighbours.add((Triangle) triangle.clone());
		return neighbours;
	}

	public List<Edge> getExclusiveEdges(List<Triangle> otherIncidents) {
		/* Returns a copy of the exclusive edges, or a empty list */
		List<Edge> exclusiveEdges = new ArrayList<Edge>();
		int abFreq = 0;
		int bcFreq = 0;
		int caFreq = 0;
		
		for (Triangle triangle : otherIncidents) {
			if (triangle.haveEdge(ab))
				abFreq ++;
			if (triangle.haveEdge(bc))
				bcFreq ++;
			if (triangle.haveEdge(ca))
				caFreq ++;				
		}
		if (abFreq == 0)
			exclusiveEdges.add((Edge) ab.clone());
		if (bcFreq == 0)
			exclusiveEdges.add((Edge) bc.clone());
		if (caFreq == 0)
			exclusiveEdges.add((Edge) ca.clone());
		
		return exclusiveEdges;
	}
	
	public boolean equals(Object other) {
		//System.out.println("usando equals do triangulo");
		Triangle otherTriangle = (Triangle) other;
		return numberOfSharedVertices(otherTriangle) == 3;	
	}
	
	public Triangle clone() {
		return new Triangle(a.clone(), b.clone(), c.clone());		
	}

	public boolean shareVertices(Triangle otherTriangle) {
		return numberOfSharedVertices(otherTriangle) >= 1;
	}
	
	private int numberOfSharedVertices(Triangle otherTriangle) {
		int numberOfSharedVertices = 0;
		
		if (a.equals(otherTriangle.a) || a.equals(otherTriangle.b) || a.equals(otherTriangle.c))
			numberOfSharedVertices ++;
		if (b.equals(otherTriangle.a) || b.equals(otherTriangle.b) || b.equals(otherTriangle.c))
			numberOfSharedVertices ++;
		if (c.equals(otherTriangle.a) || c.equals(otherTriangle.b) || c.equals(otherTriangle.c))
			numberOfSharedVertices ++;
	
		return numberOfSharedVertices;
	}
	
	private boolean haveEdge(Edge edge) {
		return ab.isEquivalent(edge) || bc.isEquivalent(edge) || ca.isEquivalent(edge);		
	}
	
	private boolean shareEdge(Triangle triangle) {
		return haveEdge(triangle.ab) || 
			haveEdge(triangle.bc) ||
			haveEdge(triangle.ca);
	}

	public Circle getCircumCircle() {
		return circumCircle.clone();
	}

	public Pattern2D getA() {
		return a.clone();
	}

	public Pattern2D getB() {
		return b.clone();
	}

	public Pattern2D getC() {
		return c.clone();
	}
	
	public String toString() {
		return " a" + a.toString() + " b" + b.toString() + " c" + c.toString();
	}

	public Pattern2D getGeometricCenter() {
		return circumCircle.getCenter();
	}

	
	
}
