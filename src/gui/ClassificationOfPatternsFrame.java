package gui;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.Border;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import gui_util.*;
import geometry.*;
import pattern.*;
import delaunay.*;
import voronoi.*;


public class ClassificationOfPatternsFrame extends JFrame {
	// some constants
	public static final int frameWidth = 500;
	public static final int frameHeigth = 500;
	private static int _NumberOfRandomicPatterns = 8;
	private static int _NumberOfNDRandomicPatterns = 5;
	private static int _2dDisplayMode = 0;
	private static int _ndDisplayMode = 1;
	
	// buttons ids
	private static String _2dButton = "2dButton";
	private static String _2dRandomButton = "2dRandomButton";
	private static String _spiralOkButton = "spiralOkButton";
	private static String _spiralNoOkButton = "spiralNoOkButton";
	private static String _ndButton = "ndButton";
	private static String _ndRandomButton = "ndRandomButton";
	
	// visual components
	private Checkbox _drawCircumCirclesCheckbox;	
	private DrawPanel drawPanel;	
	private VisualPatternList itemList;
	private Container frameContainer;
	private JPanel centerPanel; // drawPanel and itemList
		// "frame internal" visual components
		private JButton voronoiButton;
		private JButton delaunayButton;
	
	// some variables
	private int selectedDisplayMode;
	private int distanceAlgorithm;
	private int theDimensionSelected;
	private int thePreviusDimensionSelected;
	private String thePressedPatternButton;
	
	/* Foram feitas listas separadas 2d e nd pois assim � mais f�cil o controle.
	 * A lista de padr�es podia ser a mesma, j� que sempre muda.
	 * J� a lista de padr�o rand�mico ficaria mais complicado se fosse a mesma, pois � acumulativa, ent�o
	 * padr�es j� adicionados no modo 2d teriam de estar nas listas nd, e padr�es j� adicionados nd, teriam 
	 * de ser desenhados no modo 2d de forma a serem simplificados, etc */
	List<Pattern> thePatternsOfFrame; // para padr�es 2d
	List<Pattern> theRandomPatternsOfFrame; // para padr�es 2d
	List<Pattern> theNDPatternsOfFrame; // para padr�es nd 
	List<Pattern> theNDRandomPatternsOfFrame; // para padr�es nd
	
	// methods
	public ClassificationOfPatternsFrame() {
		selectedDisplayMode = ClassificationOfPatternsFrame._2dDisplayMode;
		distanceAlgorithm = -1;
		theDimensionSelected = 0;
		thePreviusDimensionSelected = 0;
		thePatternsOfFrame = new ArrayList<Pattern>();
		theRandomPatternsOfFrame = new ArrayList<Pattern>();
		theNDPatternsOfFrame = new ArrayList<Pattern>();
		theNDRandomPatternsOfFrame = new ArrayList<Pattern>();
		
		
		frameContainer = getContentPane();
		setSize(ClassificationOfPatternsFrame.frameWidth + 83, ClassificationOfPatternsFrame.frameHeigth / 2 + 80);
		setTitle(UserInterfaceConstants.titleOfMainFrame);
		createLayoutManager();
		createCenterPanel();
		createAlgorithmPanel(); // east
		createPatternsPanel(); // west		
		createAlgorithmDistanceSelectionPanel(); // south
		createResetButton(); // north
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);		
	}
	
	private void resetFrame() {
		thePatternsOfFrame = new ArrayList<Pattern>();
		theRandomPatternsOfFrame = new ArrayList<Pattern>();	
		theNDPatternsOfFrame = new ArrayList<Pattern>();
		theNDRandomPatternsOfFrame = new ArrayList<Pattern>();
		drawPanel.clear();		
		itemList.removeAll();
	}
	
	private void createLayoutManager() {
		frameContainer.setLayout(new BorderLayout());		
	}
	
	private Checkbox createDrawCircumCirclesCheckbox() {
		_drawCircumCirclesCheckbox = new Checkbox("Desenhar os circumcirculos");
		return _drawCircumCirclesCheckbox;
	}
	
	private void createItemList() {
		itemList = new VisualPatternList();
		itemList.setVisible(false);
	}

	// buttons Creation - begin
	private JButton create2dButton() {
		JButton button = new JButton("2D"); 
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (selectedDisplayMode != ClassificationOfPatternsFrame._2dDisplayMode)
					switchDisplayMode();
				List<Pattern>  _2dPatternsSet = PatternsSetFactory.getInstance().create2dPatternsSetWith14Patterns();
				removeThe2dRandomPatterns(); // acho que � importante limpar os randomicos
				drawPanel.drawPatterns(_2dPatternsSet);
				thePatternsOfFrame = _2dPatternsSet;
				thePressedPatternButton = ClassificationOfPatternsFrame._2dButton;
			}
		}); 
		return button;
	}
	
	private JButton createNnButton() {
		JButton buttonNN = new JButton("NN"); 
		buttonNN.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (selectedDistanceAlgorithm()) {
					if (selectedDisplayMode == ClassificationOfPatternsFrame._2dDisplayMode) {
						executeNnFor2DPatterns();						
					}
					else {
						executeNnForNDPatterns();
					}					
				}
				else {
					JOptionPane.showMessageDialog(null, UserInterfaceConstants.distanceAlgorithmNotSelectedMsg, "Alert", JOptionPane.ERROR_MESSAGE);
				}
			}
			
			public void executeNnFor2DPatterns() {
				assert((thePatternsOfFrame.size()) != 0);
				for (Pattern pattern : theRandomPatternsOfFrame) {					
					pattern.nearestNeighbour(thePatternsOfFrame, distanceAlgorithm);					
				}				
				drawPanel.drawPatterns(theRandomPatternsOfFrame);
			}
			
			public void executeNnForNDPatterns() {
				assert((theNDPatternsOfFrame.size()) != 0);
				for (Pattern pattern : theNDRandomPatternsOfFrame) {					
					pattern.nearestNeighbour(theNDPatternsOfFrame, distanceAlgorithm);					
				}
				itemList.removeAll(); // feio, mas fazer o que?
				itemList.addPatterns(theNDPatternsOfFrame, "P"); // feio, mas fazer o que?
				itemList.addPatterns(theNDRandomPatternsOfFrame, "A");
			}
			
		}); 
		return buttonNN;
	}
	
	private JButton createKnnButton() {
		JButton buttonKnn = new JButton("KNN"); 
		buttonKnn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (selectedDistanceAlgorithm()) {
					int k = getConstant("");
					if (k > 0) {
						if (selectedDisplayMode == ClassificationOfPatternsFrame._2dDisplayMode)
							executeKnnFor2DPatterns(k);
						else
							executeKnnForNDPatterns(k);
					}
				}
				else {
					JOptionPane.showMessageDialog(null, UserInterfaceConstants.distanceAlgorithmNotSelectedMsg, "Alert", JOptionPane.ERROR_MESSAGE);
				}
			}
			
			public void executeKnnFor2DPatterns(int k) {
				assert((thePatternsOfFrame.size()) != 0);
				for (Pattern pattern : theRandomPatternsOfFrame) {					
					try {
						pattern.kNearestNeighbour(thePatternsOfFrame, distanceAlgorithm, k);
					} catch (ExceptionKOutOfBounds e1) {
						JOptionPane.showMessageDialog(null, UserInterfaceConstants.kOfkNearestNeighbourOutOfBoundsMsg, "Alert", JOptionPane.ERROR_MESSAGE);
						break;
					}					
				}
				drawPanel.drawPatterns(theRandomPatternsOfFrame);
			}
			
			public void executeKnnForNDPatterns(int k) {
				assert((theNDPatternsOfFrame.size()) != 0);
				for (Pattern pattern : theNDRandomPatternsOfFrame) {					
					try {
						pattern.kNearestNeighbour(theNDPatternsOfFrame, distanceAlgorithm, k);
					} catch (ExceptionKOutOfBounds e1) {
						JOptionPane.showMessageDialog(null, UserInterfaceConstants.kOfkNearestNeighbourOutOfBoundsMsg, "Alert", JOptionPane.ERROR_MESSAGE);
						break;
					}					
				}
				itemList.removeAll(); // feio, mas fazer o que?
				itemList.addPatterns(theNDPatternsOfFrame, "P"); // feio, mas fazer o que?
				itemList.addPatterns(theNDRandomPatternsOfFrame, "A");						
			}
		}); 
		return buttonKnn;
	}
	
	private JButton create2dRandomButton() {
		JButton button = new JButton("2D Rand�mico"); 
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (selectedDisplayMode != ClassificationOfPatternsFrame._2dDisplayMode)
					switchDisplayMode();
				int numberOfPatterns = ClassificationOfPatternsFrame._NumberOfRandomicPatterns;
				List<Pattern>  _2dRandomPatternsSet = PatternsSetFactory.getInstance().create2dRandomPatternsSet(numberOfPatterns);
				drawPanel.drawPatterns(_2dRandomPatternsSet);
				theRandomPatternsOfFrame.addAll(_2dRandomPatternsSet);
				thePressedPatternButton = ClassificationOfPatternsFrame._2dRandomButton;
			}
		}); 
		return button;		
	}
	
	private JButton createNdButton() {
		JButton button = new JButton("ND");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (selectedDisplayMode != ClassificationOfPatternsFrame._ndDisplayMode)
					switchDisplayMode();
				thePreviusDimensionSelected = theDimensionSelected;
				theDimensionSelected = getConstant(" para a dimensao");
				if (theDimensionSelected > 0) {
					List<Pattern>  _ndPatternsSet = PatternsSetFactory.getInstance().createNDPatternsSet(theDimensionSelected);
					itemList.removeAll();
					itemList.addPatterns(_ndPatternsSet, "P"); 
					theNDPatternsOfFrame = _ndPatternsSet;
					theNDRandomPatternsOfFrame = new ArrayList<Pattern>(); // deve fazer isto ou dimens�es diferentes ir�o se misturar
					thePressedPatternButton = ClassificationOfPatternsFrame._ndButton;
				}
				if (theDimensionSelected < 0) // � negativa se a pessoa apertar "cancel"
					theDimensionSelected = thePreviusDimensionSelected;
			}
		});
		return button;		
	}
	
	private JButton createNdRandomButton() {
		JButton button = new JButton("ND Rand�mico"); 
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (theDimensionSelected == 0) {
					JOptionPane.showMessageDialog(null, "voc� antes deve gerar os padr�es ND", "Alert", JOptionPane.ERROR_MESSAGE);
				}
				else {
					/*if((theDimensionSelected != thePreviusDimensionSelected) && (thePreviusDimensionSelected != 0)) {
						theNDRandomPatternsOfFrame = new ArrayList<Pattern>();
						itemList.removeAll();
						thePreviusDimensionSelected = theDimensionSelected;
						n�o precisa, pois ND j� limpa tudo... 
					}*/
					
					if (selectedDisplayMode != ClassificationOfPatternsFrame._ndDisplayMode)
						switchDisplayMode();
					
					int numberOfPatterns = ClassificationOfPatternsFrame._NumberOfNDRandomicPatterns;
					List<Pattern>  _ndRandomPatternsSet = PatternsSetFactory.getInstance().createNDRandomPatternsSet(numberOfPatterns, theDimensionSelected);
					itemList.addPatterns(_ndRandomPatternsSet, "A");
					theNDRandomPatternsOfFrame.addAll(_ndRandomPatternsSet);  
					thePressedPatternButton = ClassificationOfPatternsFrame._ndRandomButton;
				}
			}
		}); 
		return button;		
	}	
	
	private JButton createSpiralOkButton() {
		JButton button = new JButton("Espiral");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (selectedDisplayMode != ClassificationOfPatternsFrame._2dDisplayMode)
					switchDisplayMode();
				int numberOfPatterns = getConstant(" menor igual a 900"); 
				List<Pattern>  _2dPatternsSet = PatternsSetFactory.getInstance().createSpiralOk(numberOfPatterns);
				removeThe2dRandomPatterns(); // acho que � importante limpar os randomicos
				drawPanel.drawPatterns(_2dPatternsSet);
				thePatternsOfFrame = _2dPatternsSet;
				thePressedPatternButton = ClassificationOfPatternsFrame._spiralOkButton;
			}
		});
		return button;		
	}
	
	private JButton createSpiralNoOkButton() {
		JButton button = new JButton("Espiral com ru�do");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (selectedDisplayMode != ClassificationOfPatternsFrame._2dDisplayMode)
					switchDisplayMode();
				int numberOfPatterns = getConstant(" menor igual a 900"); 
				List<Pattern>  _2dPatternsSet = PatternsSetFactory.getInstance().createSpiralNoOk(numberOfPatterns);
				removeThe2dRandomPatterns(); // acho que � importante limpar os randomicos
				drawPanel.drawPatterns(_2dPatternsSet);
				thePatternsOfFrame = _2dPatternsSet;
				thePressedPatternButton = ClassificationOfPatternsFrame._spiralNoOkButton;
			}
		});
		return button;		
	}
	
	private JButton createDelaunayButton() {
		JButton button = new JButton("Delaunay");
		delaunayButton = button; // :)
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<Triangle> triangles;				
				triangles = executeDelaunayTriangulation();				
				drawPanel.drawTriangles(triangles, drawCircumCircles());
				//System.out.println("number of triangles " + triangles.size());
			}			
		});
		return button;		
	}
	
	private JButton createVoronoiButton() {
		JButton button = new JButton("Voronoi");
		voronoiButton = button; // :)
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VoronoiDiagram voronoiDiagram = executeVoronoiDiagramation();
				Iterator centroidsIterator = voronoiDiagram.getCentroidsIterator();
				List<Pattern> centroids = new ArrayList<Pattern>();
				while (centroidsIterator.hasNext()) {
					Pattern2D centroid = (Pattern2D) centroidsIterator.next();
					centroids.add(centroid);
				}
				Iterator edgesIterator = voronoiDiagram.getEdgesIterator();
				List<Edge> edges = new ArrayList<Edge>();
				while (edgesIterator.hasNext()) {
					Edge edge = (Edge) edgesIterator.next();
					edges.add(edge);
				}
				drawPanel.drawPatterns(centroids);
				drawPanel.drawEdges(edges);				
			}			
		});
		return button;		
	}
	
	private void createResetButton() {
		JButton button = new JButton("Reset");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resetFrame();
			}
		});
		frameContainer.add(button, BorderLayout.NORTH);
	}
	
	// buttons Creation - end
	
	
	// panels  Creation - begin
	private void createCenterPanel() {
		centerPanel = new JPanel();
		centerPanel.setLayout(new BorderLayout());
		createPenAndDrawPanel(); // center
		createItemList(); // center :) come�a invis�vel e sem ser inserido no painel central
		centerPanel.add(drawPanel, BorderLayout.CENTER);
		frameContainer.add(centerPanel, BorderLayout.CENTER);
	}
	
	private void createPenAndDrawPanel() {
		int den = 2;
		int width = ClassificationOfPatternsFrame.frameWidth / den;
		int heigth = ClassificationOfPatternsFrame.frameHeigth / den;
		Pen aPen = new Pen(width, heigth);
		drawPanel = new DrawPanel(width, heigth, aPen);
		
		//	A bord to the panel
		//Border lineBorder = BorderFactory.createLineBorder(Color.black);
		//Border border = BorderFactory.createTitledBorder(lineBorder, "Display");
		//drawPanel.setBorder(border);
		
		PatternsSetFactory.getInstance().setConstants(Math.min(width, heigth));
	}
	
	private void createAlgorithmPanel() {
		JPanel algorithmPanel = new JPanel();
		algorithmPanel.setLayout(new GridLayout(5,1));
		algorithmPanel.add(createNnButton());
		algorithmPanel.add(createKnnButton());
		algorithmPanel.add(createDelaunayButton());
		algorithmPanel.add(createVoronoiButton());
		algorithmPanel.add(createDrawCircumCirclesCheckbox());
		frameContainer.add(algorithmPanel, BorderLayout.EAST);		
	}	

	private void createPatternsPanel() {
		JPanel patternsPanel = new JPanel();
		patternsPanel.setLayout(new GridLayout(6,1));
		patternsPanel.add(create2dButton());
		patternsPanel.add(create2dRandomButton());
		patternsPanel.add(createSpiralOkButton());
		patternsPanel.add(createSpiralNoOkButton());
		patternsPanel.add(createNdButton());
		patternsPanel.add(createNdRandomButton());
		frameContainer.add(patternsPanel, BorderLayout.WEST);
	}
	
	private void createAlgorithmDistanceSelectionPanel() {
		JPanel algorithmDistanceSelectionPanel = new JPanel();
		ButtonGroup options = new ButtonGroup(); 
		JRadioButtonMenuItem euclidianButton = new JRadioButtonMenuItem("Dist�ncia Euclidiana");
		JRadioButtonMenuItem hammingButton = new JRadioButtonMenuItem("Dist�ncia Hamming");
		options.add(euclidianButton);
		options.add(hammingButton);
		algorithmDistanceSelectionPanel.add(euclidianButton);
		algorithmDistanceSelectionPanel.add(hammingButton);
		frameContainer.add(algorithmDistanceSelectionPanel, BorderLayout.SOUTH);
		// Behavior for the buttons
		euclidianButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//System.out.println("euclidian selected");
				distanceAlgorithm = Pattern.euclidianDistance;
			}
		});
		
		hammingButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//System.out.println("hamming selected");
				distanceAlgorithm = Pattern.hammingDistance;
			}
		});
	}
	
	// panels  Creation - end
	
	private void removeThe2dRandomPatterns() {
		drawPanel.clear();
		theRandomPatternsOfFrame = new ArrayList<Pattern>();		
	}
	
	private void switchDisplayMode() {
		if (drawPanel.isVisible()) {
			// 2d ---> nd
			drawPanel.setVisible(false);
			centerPanel.remove(drawPanel);
			delaunayButton.setEnabled(false);
			voronoiButton.setEnabled(false);
						
			selectedDisplayMode = ClassificationOfPatternsFrame._ndDisplayMode; 
			
			centerPanel.add(itemList, BorderLayout.CENTER);
			itemList.setVisible(true);						
		}
		else {
			// nd ---> 2d
			itemList.setVisible(false);					
			centerPanel.remove(itemList);
								
			selectedDisplayMode = ClassificationOfPatternsFrame._2dDisplayMode;
			
			delaunayButton.setEnabled(true);
			voronoiButton.setEnabled(true);
			centerPanel.add(drawPanel, BorderLayout.CENTER);
			drawPanel.setVisible(true);														
		}
	}
	
	private boolean drawCircumCircles() {
		return _drawCircumCirclesCheckbox.getState();
	}
	
	private boolean selectedDistanceAlgorithm() {
		return (distanceAlgorithm == Pattern.euclidianDistance) ||
				(distanceAlgorithm == Pattern.hammingDistance);
	}
	
	private int getConstant(String extraMsg) {
		int k = -1;
		String s = JOptionPane.showInputDialog(UserInterfaceConstants.numberRequestMsg + extraMsg);
		try {
			k= (new Integer(s)).intValue(); 
			if (k <= 0)
				throw new NumberFormatException();
		}
		catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, UserInterfaceConstants.numberFormatErrorMsg, "Alert", JOptionPane.ERROR_MESSAGE);
		}
		return k;
	}
	
	private List<Triangle> executeDelaunayTriangulation() {
		List<Triangle> triangles = null;
		//thePressedPatternButton = ""; //retirar		
		
		DelaunayIncrementalTriangulator triangulator = new DelaunayIncrementalTriangulator();
		assert((thePatternsOfFrame.size()) != 0 || (theRandomPatternsOfFrame.size()) != 0);
		if (thePressedPatternButton.equals(ClassificationOfPatternsFrame._2dButton)) { 
			triangles = triangulator.triangulize(thePatternsOfFrame);
		}				
		else if (thePressedPatternButton.equals(ClassificationOfPatternsFrame._2dRandomButton)) {
			triangles = triangulator.triangulize(theRandomPatternsOfFrame);
		}
		else if (thePressedPatternButton.equals(ClassificationOfPatternsFrame._spiralOkButton)) {
			triangles = triangulator.triangulize(thePatternsOfFrame);
		}
		else if (thePressedPatternButton.equals(ClassificationOfPatternsFrame._spiralNoOkButton)) {
			triangles = triangulator.triangulize(thePatternsOfFrame);
		}
		//else {
			/* apenas para testes, pode retirar */
			//	triangles = triangulator.triangulize(PatternsSetFactory.getInstance().create2dPatternsSetWith14Patterns());
		//}
		
		return triangles;
	}
	
	private VoronoiDiagram executeVoronoiDiagramation() {
		VoronoiDiagramator voronoiDiagramator = new VoronoiDiagramator(); 
		List<Triangle> triangles = executeDelaunayTriangulation();
		
		VoronoiDiagram voronoiDiagram = voronoiDiagramator.diagramize(triangles);
		return voronoiDiagram;
	}
	
	public static void main(String[] args) {
		ClassificationOfPatternsFrame canvas = new ClassificationOfPatternsFrame();
	}
}
