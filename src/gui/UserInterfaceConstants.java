package gui;

public class UserInterfaceConstants {
	public static final String titleOfMainFrame = "Classificação de Padrões - por Mateus e Nuno";
	public static final String distanceAlgorithmNotSelectedMsg =  "O algoritmo de distancia nao foi selecionado, por favor escolha um"; //"The distance algorithm has not be selected, please choose one"
	public static final String numberFormatErrorMsg = "entre com um numero maior que zero"; //"type a number"
	public static final String numberRequestMsg = "Informe um numero natural maior que zero"; //"Give a natural number"
	public static final String kOfkNearestNeighbourOutOfBoundsMsg = "O k deve ser menor ou igual a quantidade de padroes"; //The k must be less or equal to the number of patterns"
}
