package gui_util;
import java.awt.Graphics;
import java.util.List;
import geometry.*;
import pattern.*;

import javax.swing.JPanel;

public class DrawPanel extends JPanel {
	/* O painel que aparece na GUI e que mostra
	 * o desenho da Caneta.
	 * � tamb�m quem pede para a caneta desenhar
	 * 
	 * */
	
	private Pen aPen;
	
	public DrawPanel(int width, int height, Pen pen) {
		setSize(width, height);
		setVisible(true);		
		aPen = pen;
	}
	
	public void paint(Graphics g) {
		super.paint(g);
		g.drawImage(aPen.getBuffer(), 0, 0, this);				
	}

	public void drawPatterns(List<Pattern> patternsSet) {
		for (Pattern pattern : patternsSet) {
			drawPattern(pattern);
		}
		repaint();
	}

	private void drawPattern(Pattern pattern) {
		int dimensionValues[] = new int[2];
		int i = 0;
		
		for(pattern.firstDimensionValue(); ! pattern.isDone(); pattern.nextDimensionValue()) {
			dimensionValues[i] = pattern.currentDimensionValue().intValue();
			//System.out.println("i: " + i);
			//System.out.println("dimensionValues[i]: " + dimensionValues[i]);
			i ++;			
		}
		aPen.setColor(pattern.getClassName());
		aPen.drawPoint(dimensionValues[0], dimensionValues[1]);
	}

	public void clear() {
		aPen.clear();
		repaint();		
	}

	public void drawTriangles(List<Triangle> triangles, boolean drawCircumCircles) {
		//System.out.println("Executing DrawPanel::drawTriangles");
		for (Triangle t : triangles) {
			//System.out.println("DrawPanel::drawTriangles t= " + t.toString());
			aPen.drawTriangle(t, drawCircumCircles);
		}		
		repaint();
	}
	
	public void drawEdges(List<Edge> edges) {
		for (Edge edge : edges) {
			aPen.drawEdge(edge);
		}
	}
	
}
