package gui_util;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Hashtable;
import java.util.Map;
import geometry.*;
import pattern.*;

public class Pen {
	/* Caneta que possui um graphicBuffer e desenha
	 * em si mesma usando coordenadas cartezianas */
	
	private Graphics graphicBuffer;
	private Image imageBuffer;
	private int imageBufferWidth;
	private int imageBufferHeigth;
	
	Map<String, Color> dicColors;
	static int pixelSize = 8;
	Color initialColor;
	
	public Pen(int width, int height) {
		registerColors();
		imageBuffer = new BufferedImage (width, height, BufferedImage.BITMASK);
		graphicBuffer = imageBuffer.getGraphics();
		initialColor = graphicBuffer.getColor();
		imageBufferWidth = imageBuffer.getWidth(null);
		imageBufferHeigth = imageBuffer.getHeight(null);	
		clear();		
	}
	
	private void registerColors() {
		dicColors = new Hashtable<String, Color>();
		dicColors.put("red", Color.red);
		dicColors.put("orange", Color.orange);
		dicColors.put("yellow", Color.yellow);
		dicColors.put("green", Color.green);
		dicColors.put("blue", Color.blue);
		dicColors.put("cyan", Color.cyan);
		dicColors.put("black", Color.black);
		dicColors.put("white", Color.white);
		dicColors.put("pink", Color.pink);
		dicColors.put("gray", Color.gray);
		dicColors.put("darkGray", Color.darkGray);		
		dicColors.put("magenta", Color.magenta);
	}

	public void setColor(String className) {
		Color color;
		String colorName;
		
		colorName = className.toLowerCase();
		color = dicColors.get(colorName);
		graphicBuffer.setColor(color);		
	}
	
	private void drawLines() {
		/* Desenha os eixos X e Y do plano cartesiano */
		graphicBuffer.setColor(Color.blue);
		graphicBuffer.drawLine(imageBufferWidth / 2, 0, imageBufferWidth / 2, imageBufferHeigth);
		graphicBuffer.drawLine(0, imageBufferHeigth / 2, imageBufferWidth, imageBufferHeigth / 2);
	}

	public void drawPoint(int x, int y){
		int nx = getNewX(x);
		int ny = getNewY(y);
		//graphicBuffer.setColor(Color.black);
		int diameter = 2 * defaultRadius();
		graphicBuffer.fillOval(nx - defaultRadius(), ny - defaultRadius(), diameter, diameter);
		//System.out.println("nx: " + nx);
		//System.out.println("ny: " + ny);
	}
	
	private int getNewX(int x) {
		int halfWidth = imageBufferWidth / 2;
		int diameter = 2 * defaultRadius();
		//if (Math.abs(x) > halfWidth / diameter)
			//System.out.println("x out of bounds");		
		
		return (x * diameter + imageBufferWidth / 2);
	}
	
	private int getNewY(int y) {
		int halfHeigth = imageBufferHeigth / 2;
		int diameter = 2 * defaultRadius();
		//if (Math.abs(y) > halfHeigth / diameter)
			//System.out.println("y out of bounds");
		
		return -y * diameter + imageBufferHeigth / 2;
	}
		
	private int defaultRadius() {
		return pixelSize / 2;
	}

	public Image getBuffer() {
		return imageBuffer;
	}

	public void clear() {
		graphicBuffer.clearRect(0, 0, imageBufferWidth, imageBufferHeigth);
		graphicBuffer.setColor(initialColor);
		graphicBuffer.fillRect(0, 0, imageBufferWidth, imageBufferHeigth);
		drawLines();		
	}

	public void drawTriangle(Triangle t, boolean drawCircumCircles) {
		Pattern2D a = (Pattern2D) t.getA();
		Pattern2D b = (Pattern2D) t.getB();
		Pattern2D c = (Pattern2D) t.getC();
		int xa, xb, xc, ya, yb, yc;
		xa = getNewX((int) a.getX());
		xb = getNewX((int) b.getX());
		xc = getNewX((int) c.getX());
		ya = getNewY((int) a.getY());
		yb = getNewY((int) b.getY());
		yc = getNewY((int) c.getY());
		graphicBuffer.setColor(Color.black);
		graphicBuffer.drawLine(xa, ya, xb, yb);
		graphicBuffer.drawLine(xb, yb, xc, yc);
		graphicBuffer.drawLine(xc, yc, xa, ya);		
		if (drawCircumCircles)
			drawCircle(t.getCircumCircle());
	}

	public void drawCircle(Circle circumCircle) {
		Pattern2D center = circumCircle.getCenter();
		int radius = (int) circumCircle.getRadius() * pixelSize;
		int diameter = radius * 2;
		int xc = getNewX((int) center.getX());
		int yc = getNewY((int) center.getY());
		graphicBuffer.setColor(Color.green);
		//Graphics2D graphicBuffer2D = (Graphics2D) graphicBuffer;  
		//graphicBuffer2D.setStroke(new BasicStroke(pixelSize/2));
		graphicBuffer.drawOval(xc - radius - defaultRadius(), yc - radius - defaultRadius(), diameter + pixelSize, diameter + pixelSize);
		//graphicBuffer2D.setStroke(new BasicStroke(1));
		//System.out.println(circumCircle.toString());
	}
	
	public void drawEdge(Edge edge) {
		int v1nx = getNewX((int) edge.getV1().getX());
		int v1ny = getNewY((int) edge.getV1().getY());
		int v2nx = getNewX((int) edge.getV2().getX());
		int v2ny = getNewY((int) edge.getV2().getY());
		graphicBuffer.setColor(Color.red);
		graphicBuffer.drawLine(v1nx, v1ny, v2nx, v2ny);		
	}
	
}
