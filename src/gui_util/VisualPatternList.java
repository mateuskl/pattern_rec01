package gui_util;
import java.awt.List;
import pattern.Pattern;;

public class VisualPatternList extends java.awt.List {
	/* Utilizada na visualiza��o de padr�es N dimensionais */

	public void addPatterns(java.util.List<Pattern> patternsSet, String signal) {
		for (Pattern pattern : patternsSet) {
			add(signal + pattern.toString());
		}		
	}

}
