package pattern;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import geometry.NDPoint;;

public class Pattern extends NDPoint {

	private String className;
	
	public Pattern(double[] dimensionsValues, String className) {
		super(dimensionsValues);		
		setClassName(className);
	}
	
	public Pattern() {
		super();
	}

	public String getClassName() {
		return className;
	}
	
	void setClassName(String value) {
		className = value;
	}
	
	// ClassificationAlgorithms
		// nearestNeighbour
	public String nearestNeighbour(List<Pattern> setOfPatterns, int distanceAlgorithmName) {
		Pattern nearestNeighbour;
		int index = -1;
		
		switch (distanceAlgorithmName) {
			case NDPoint.hammingDistance:
				index = nearestNeighbourHammingDistance(setOfPatterns);				
				break;
			
			case NDPoint.euclidianDistance:
				index = nearestNeighbourEuclidianDistance(setOfPatterns);
				break;

			default:
				break;
		}		
		nearestNeighbour = setOfPatterns.get(index);
		setClassName(nearestNeighbour.getClassName());
		return getClassName();
	}
	
		// kNearestNeighbour
	public String kNearestNeighbour(List<Pattern> setOfPatterns, int distanceAlgorithmName, int k) throws ExceptionKOutOfBounds {
		if (k > setOfPatterns.size())
			throw new ExceptionKOutOfBounds();
		
		switch (distanceAlgorithmName) {
		case NDPoint.hammingDistance:
			setClassName(kNearestNeighbourHammingDistance(setOfPatterns, k));
			break;
		
		case NDPoint.euclidianDistance:
			setClassName(kNearestNeighbourEuclidianDistance(setOfPatterns, k));
			break;
			
		default:
			break;
		}
	
		return getClassName();
	}

	// private Methods
		// nearestNeighbour
	private int nearestNeighbourHammingDistance(List<Pattern> setOfPatterns) {
		int nearestNeighbourIndex = 0;
		Pattern nearestNeighbour = setOfPatterns.get(nearestNeighbourIndex);
		//String nearestNeighbourClassName = nearestNeighbour.getClassName();
		double nearestNeighbourDistance = hammingDistance((NDPoint) nearestNeighbour);
				
		for (int i = 1; i < setOfPatterns.size(); ++ i) {
			double distance = hammingDistance((NDPoint) setOfPatterns.get(i));
			if (distance < nearestNeighbourDistance) {
				nearestNeighbour = setOfPatterns.get(i);
				//nearestNeighbourClassName = nearestNeighbour.getClassName();
				nearestNeighbourDistance = distance;
				nearestNeighbourIndex = i;
			}			
		}
		
		return nearestNeighbourIndex;		
		//return nearestNeighbourClassName;
	}
	
	private int nearestNeighbourEuclidianDistance(List<Pattern> setOfPatterns) {
		int nearestNeighbourIndex = 0;
		Pattern nearestNeighbour = setOfPatterns.get(nearestNeighbourIndex);
		//String nearestNeighbourClassName = nearestNeighbour.getClassName();
		double nearestNeighbourDistance = euclidianDistance((NDPoint) nearestNeighbour);
				
		for (int i = 1; i < setOfPatterns.size(); ++ i) {
			double distance = euclidianDistance((NDPoint) setOfPatterns.get(i));
			if (distance < nearestNeighbourDistance) {
				nearestNeighbour = setOfPatterns.get(i);
				//nearestNeighbourClassName = nearestNeighbour.getClassName();
				nearestNeighbourDistance = distance;
				nearestNeighbourIndex = i;
			}
		}		
		
		return nearestNeighbourIndex;		
		//return nearestNeighbourClassName;		
	}
	
		//	kNearestNeighbour
	private String kNearestNeighbourHammingDistance(List<Pattern> setOfPatterns, int k) {
		List<Pattern> patterns = copysetOfPatterns(setOfPatterns);
		List<Pattern> kNearestNeighbours = new ArrayList<Pattern>();
		Pattern pattern;
		int index = -1;
				
		for (int i = 0; i < k; ++ i) {
			index = nearestNeighbourHammingDistance(patterns); 
			pattern = patterns.get(index);
			kNearestNeighbours.add(pattern.clone());
			patterns.remove(index);
		}
		
		return classNameOfMostFrequentPattern(kNearestNeighbours);
	}
	
	private String kNearestNeighbourEuclidianDistance(List<Pattern> setOfPatterns, int k) {
		List<Pattern> patterns = copysetOfPatterns(setOfPatterns);
		List<Pattern> kNearestNeighbours = new ArrayList<Pattern>();
		Pattern pattern;
		int index = -1;
				
		for (int i = 0; i < k; ++ i) {
			index = nearestNeighbourEuclidianDistance(patterns); 
			pattern = patterns.get(index);
			kNearestNeighbours.add(pattern.clone());
			patterns.remove(index);
		}
		
		return classNameOfMostFrequentPattern(kNearestNeighbours);
	}
	
	
	// Other methods
	
	private String classNameOfMostFrequentPattern(List<Pattern> nearestNeighbours) {
		// varre a lista se o padr�o n�o existe cadastra, se sim incrementa frequencia
		Map<String, Integer> classNamesFrequencies = new HashMap<String, Integer>();
		
		for (Pattern pattern : nearestNeighbours) {
		    if (! classNamesFrequencies.containsKey(pattern.getClassName()))
		    		classNamesFrequencies.put(pattern.getClassName(), new Integer(1));
		    else {
		    	Integer aux = classNamesFrequencies.get(pattern.getClassName());
		    	aux = new Integer(aux.intValue() + 1);
		    	classNamesFrequencies.put(pattern.getClassName(), aux);
		    }
		}
		
		Set<Map.Entry<String, Integer>>  frequencies = classNamesFrequencies.entrySet();
		String classNameOfMostFrequentPattern = "";
		int maxFreq = 0;
		
		for (Map.Entry<String, Integer> entry : frequencies) {
		    if (entry.getValue() > maxFreq) {
		    	maxFreq = entry.getValue();
		    	classNameOfMostFrequentPattern = entry.getKey();
		    }
		}
		
		return classNameOfMostFrequentPattern;
	}
	
	private List<Pattern> copysetOfPatterns(List<Pattern> setOfPatterns) {
		List<Pattern> patterns = new ArrayList<Pattern>();
		
		for (int i = 0; i < setOfPatterns.size(); ++ i) {
			patterns.add(((Pattern) ((Pattern) setOfPatterns.get(i)).clone()));
		}
		return patterns;
	}	
	
	public Pattern clone() {
		double dimensionsValues[] = new double[dimensions.size()];
		
		for (int i = 0; i < dimensions.size(); ++ i) {
			dimensionsValues[i] = dimensions.get(i).doubleValue();
		}
		
		return new Pattern(dimensionsValues, getClassName());
	}
	
	public boolean equals(Object other) {
		Pattern otherPatern = (Pattern) other;
		return isEquals(otherPatern) && className.equals(otherPatern.className);		
	}
	
	public String toString() {
		NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
		nf.setMaximumFractionDigits(2);
	    String stringPattern = "(";
		for (Double dimension : dimensions) {
			String strDimension = nf.format(dimension.doubleValue());
			stringPattern += strDimension + ", ";
		}
		stringPattern += getClassName() + ")";
		return stringPattern;
	}
	
}
