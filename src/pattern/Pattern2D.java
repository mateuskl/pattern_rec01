package pattern;
import java.util.Vector;

public class Pattern2D extends Pattern {

	public Pattern2D (double x, double y, String className) {
		super();
		dimensions = new Vector<Double>();
		dimensions.add(new Double(x));
		dimensions.add(new Double(y));
		currentDimensionValueIndex = 0;
		setClassName(className);		
	}
	
	public double getX() {
		return dimensions.get(0).doubleValue();
	}

	public double getY() {
		return dimensions.get(1).doubleValue();
	}
	
	public Pattern2D clone() {
		return new Pattern2D(getX(), getY(), getClassName());
	}	
	
}
