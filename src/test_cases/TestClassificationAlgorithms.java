package test_cases;
import java.util.ArrayList;
import java.util.List;
import geometry.*;
import pattern.*;

import junit.framework.TestCase;

public class TestClassificationAlgorithms extends TestCase {
	
	private Pattern a;
	private Pattern b;
	private Pattern c;
	private Pattern d;
	private Pattern e;
	private Pattern newPattern;
	private List<Pattern> setOfPatterns;
	
	protected void setUp() throws Exception {
		double aDimensionsValues[] = {0.0, 1.0};
		double bDimensionsValues[] = {5.0, 2.0};
		double cDimensionsValues[] = {-4.0, -3.0};
		double dDimensionsValues[] = {2.0, 0.0};
		double eDimensionsValues[] = {0.0, -3.0};
		double newPatternDimensionsValues[] = {0.0, 0.0};
		
		a = new Pattern(aDimensionsValues, "A class");
		b = new Pattern(bDimensionsValues, "B class");
		c = new Pattern(cDimensionsValues, "C class");
		d = new Pattern(dDimensionsValues, "X class");
		e = new Pattern(eDimensionsValues, "X class");
		newPattern = new Pattern(newPatternDimensionsValues, "");
		
		setOfPatterns = new ArrayList<Pattern>();
		setOfPatterns.add(a);
		setOfPatterns.add(b);
		setOfPatterns.add(c);
		setOfPatterns.add(d);
		setOfPatterns.add(e);
	}
	
	public void testNearestNeighbourEuclidianDistance() {
		String patternClassName = newPattern.nearestNeighbour(setOfPatterns, NDPoint.euclidianDistance);
		assertEquals("A class", patternClassName);
	}

	public void testNearestNeighbourHammingDistance() {
		String patternClassName = newPattern.nearestNeighbour(setOfPatterns, NDPoint.hammingDistance);
		assertEquals("A class", patternClassName);
	}
	
	public void testK_NearestNeighbourEuclidianDistance() {
		int k = 3;
		String patternClassName = null;
		try {
			patternClassName = newPattern.kNearestNeighbour(setOfPatterns, NDPoint.euclidianDistance, k);
		} catch (ExceptionKOutOfBounds e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals("X class", patternClassName);
	}	
	
	public void testK_NearestNeighbourHammingDistance() {
		int k = 3;
		String patternClassName = null;
		try {
			patternClassName = newPattern.kNearestNeighbour(setOfPatterns, NDPoint.hammingDistance, k);
		} catch (ExceptionKOutOfBounds e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals("X class", patternClassName);
	}
	
	public void testWhenA_PatternIsClassifiedItsClassNameChanges() {
		int k = 3;
		
		newPattern.nearestNeighbour(setOfPatterns, NDPoint.euclidianDistance);
		assertEquals("A class", newPattern.getClassName());
		
		try {
			newPattern.kNearestNeighbour(setOfPatterns, NDPoint.euclidianDistance, k);
		} catch (ExceptionKOutOfBounds e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		assertEquals("X class", newPattern.getClassName());
		
		newPattern.nearestNeighbour(setOfPatterns, NDPoint.hammingDistance);
		assertEquals("A class", newPattern.getClassName());
		
		try {
			newPattern.kNearestNeighbour(setOfPatterns, NDPoint.hammingDistance, k);
		} catch (ExceptionKOutOfBounds e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals("X class", newPattern.getClassName());		
	}
	
	public void testTheK_CantBeGreatherThenTheNumberOfPatternsOfTheSet() {
		int k = setOfPatterns.size() + 1;
		try {
			newPattern.kNearestNeighbour(setOfPatterns, NDPoint.euclidianDistance, k);
			assertFalse(true);
		}
		catch (ExceptionKOutOfBounds e) {
			
		}		
	}
		
}
