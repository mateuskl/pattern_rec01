package test_cases;

import geometry.*;
import junit.framework.TestCase;

public class TestDistanceAlgorithms extends TestCase {
	
	private NDPoint a;
	private NDPoint b;
	
	protected void setUp() throws Exception {
		double aDimensionsValues[] = {5.0, 7.0};
		double bDimensionsValues[] = {4.0, 1.0};
		
		a = new NDPoint(aDimensionsValues);
		b = new NDPoint(bDimensionsValues);
	}
	
	public void testEuclidianDistance() {
		double distance = a.euclidianDistance(b);		
		assertTrue(distance == Math.sqrt(37.0));		
	}
	
	public void testHammingDistance() {
		double distance = a.hammingDistance(b);
		assertTrue(distance == 7.0);		
	}

}
