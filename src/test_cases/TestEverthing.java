package test_cases;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TestEverthing extends TestCase {
	public static void main(String[] args) {
		Test all = setOfAllTests();
		System.out.println(all.countTestCases());
		junit.textui.TestRunner.run(all);
		//junit.textui.TestRunner.run(setOfAllTests());
	}
		
	public static Test setOfAllTests() {
		TestSuite result = new TestSuite();
		// 
		result.addTest(new TestSuite(TestDistanceAlgorithms.class));
		result.addTest(new TestSuite(TestClassificationAlgorithms.class));
		//			
		return result;		
	}
	
}
