package voronoi;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import pattern.Pattern2D;
import geometry.Triangle;
import geometry.Edge;

public class VoronoiDiagram {

	List<Pattern2D> centroids;
	List<Edge> edges;
	
	public VoronoiDiagram(List<Triangle> triangulation) {
		centroids = new ArrayList<Pattern2D>();
		edges = new ArrayList<Edge>();
		
		for (Triangle triangle : triangulation) {
			if (! centroids.contains(triangle.getA()))
				centroids.add(triangle.getA());
			if (! centroids.contains(triangle.getB()))
				centroids.add(triangle.getB());
			if (! centroids.contains(triangle.getC()))
				centroids.add(triangle.getC());			
		}
	}

	public boolean contains(Edge edge) {
		return edges.contains(edge);
	}

	public void add(Edge edge) {
		edges.add(edge);		
	}
	
	public Iterator getCentroidsIterator() {
		return centroids.iterator();
	}
	
	public Iterator getEdgesIterator() {
		return edges.iterator();
	}

}
