package voronoi;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import pattern.Pattern2D;
import geometry.Triangle;
import geometry.Edge;

public class VoronoiDiagramator {
	
	private VoronoiDiagram voronoiDiagram;
	
	//int exclusivas, centrosoutros;
	
	public VoronoiDiagram diagramize(List<Triangle> triangulation) {
		//exclusivas = centrosoutros = 0;
		
		voronoiDiagram = new VoronoiDiagram(triangulation);
		
		for (Triangle triangle : triangulation) {
			List<Triangle> others = triangle.getOverYourself(triangulation);
			List<Triangle> neighbours = triangle.getNeighbours(others);
			assert(neighbours.size() == 0 || neighbours.size() == 1 || neighbours.size() == 2 || neighbours.size() == 3) : "A triangle must have 0,2 or 3 neighbours";
			//System.out.println("number of neighbours" + neighbours.size());
			
			if (isAOutsideTriangle(triangle, neighbours)) {
				conectCenterWithTheMiddlePointsOfExclusivesEdges(triangle, neighbours);
			}
			
			ifNotConectedConectCenterWithTheirNeighboursCenters(triangle, neighbours);
			
		}
		
		//System.out.println("number of edges of exclusive edges " + exclusivas);
		//System.out.println("number of edges of neighbours " + centrosoutros);
		return voronoiDiagram;		
	}

	//
	private void ifNotConectedConectCenterWithTheirNeighboursCenters(Triangle triangle, List<Triangle> neighbours) {
		Pattern2D center = triangle.getGeometricCenter();
		Edge edge = null;
		for (Triangle neighbour : neighbours) {
			edge = new Edge(center, neighbour.getGeometricCenter());
			if (! voronoiDiagram.contains(edge)) {
				voronoiDiagram.add(edge);
				//System.out.println("adicionando aresta voronoi centro vizinhos");
				//centrosoutros ++;
			}
		}		
	}

	private void conectCenterWithTheMiddlePointsOfExclusivesEdges(Triangle triangle, List<Triangle> neighbours) {
		Pattern2D center = triangle.getGeometricCenter();
		List<Edge> exclusiveEdges = triangle.getExclusiveEdges(neighbours); 
		for (Edge exclusiveEdge : exclusiveEdges) {
			Pattern2D middlePoint = exclusiveEdge.getMiddlePoint();
			// expand middlePoint - end
			middlePoint = new Pattern2D(middlePoint.getX() * 1.5, middlePoint.getY() * 1.5,""); 	
			// expand middlePoint - end
			Edge edge = new Edge(center, middlePoint);
			assert(! voronoiDiagram.contains(edge));
			voronoiDiagram.add(edge);
			//System.out.println("adicionando aresta voronoi arestas exclusivas");
			//exclusivas ++;
		}
	}	

	private boolean isAOutsideTriangle(Triangle triangle, List<Triangle> neighbours) {
		return triangle.getExclusiveEdges(neighbours).size() >= 1;
		//return numberOfNeighbours == 2;
	}		
}


/* 
 	private List<VoronoiCell> voronoiCells;
	
	public List<VoronoiCell> diagramize(List<Triangle> triangulation) {
		voronoiCells = new ArrayList<VoronoiCell>();
		generateInternalCells(triangulation);
		return voronoiCells;		
	}

	private void generateInternalCells(List<Triangle> triangulation) {
		 ligar centros dos triāngulos vizinhos 
		Circle circumCircle;
		for (Triangle triangle : triangulation) {
			neighbours = triangle.getNeighbours();
			for (Triangle n : neighbours) {
				
			}
			circumCircle = triangle.getCircumCircle();
			
		}
	}
 
 */